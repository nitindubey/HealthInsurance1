package com.health;

import java.util.Scanner;

@SuppressWarnings("resource")
public class HealthInsurance {
	
	public static void main(String[] args){
		
		Scanner sc=new Scanner(System.in);
		
		Entity en = Entity.getInstance();
		
		BasePremium base = BasePremium.getInstance();
		
		System.out.println("Enter Name:");
		String name  =sc.nextLine();
		
		System.out.println("Enter Gender:");
		en.setGender(sc.nextLine());
		
		System.out.println("Enter Age:");
		en.setAge(sc.nextInt());
		
		System.out.println("Current Health:");
		
		System.out.println("\tHypertension:");
		en.setHyperTension(sc.next());
		
		System.out.println("\tBlood pressure:");
		en.setBloodPressure(sc.next());
		
		System.out.println("\tBlood sugar:");
		en.setBloodSugar(sc.next());
		
		System.out.println("\tOverweight:");
		en.setOverWeight(sc.next());
		
		System.out.println("Habits:");
		
		System.out.println("\tSmoking:");
		en.setSmoking(sc.next());
		
		System.out.println("\tAlcohol:");
		en.setAlcohal(sc.next());
		
		System.out.println("\tDaily Exercise:");
		en.setDailyExercise(sc.next());
		
		System.out.println("\tDrugs:");
		en.setDrugs(sc.next());
		
		if(en.getGender().equalsIgnoreCase("Male")){
			System.out.println("Health Insurance Premium for Mr. "+name+": Rs. "+base.premimumCalc(en));
		}else if(en.getGender().equalsIgnoreCase("Female")){
			System.out.println("Health Insurance Premium for Mrs. "+name+": Rs. "+base.premimumCalc(en));
		}else{
			System.out.println("Health Insurance Premium for "+name+": Rs. "+base.premimumCalc(en));
		}
	}
		
}
