package com.health.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.BasePremium;
import com.health.Entity;

public class TestCase5 {

	@Test
	public void test() {
		
		Entity en = Entity.getInstance();
		
		en.setAge(37);
		en.setGender("male");
		en.setHyperTension("yes");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("yes");
		en.setAlcohal("yes");
		en.setDailyExercise("yes");
		en.setDrugs("no");
		
		BasePremium base = BasePremium.getInstance();
		
		double test=base.premimumCalc(en);
		
		//System.out.println("test5....."+test);
		assertEquals(7838.443783466044,test,0.443783466044);
	}

}
