package com.health.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.BasePremium;
import com.health.Entity;

public class TestCase4 {

	@Test
	public void test() {
		
		Entity en = Entity.getInstance();
		
		en.setAge(28);
		en.setGender("female");
		en.setHyperTension("yes");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("yes");
		en.setAlcohal("yes");
		en.setDailyExercise("no");
		en.setDrugs("no");
		
		BasePremium base = BasePremium.getInstance();
		
		double test=base.premimumCalc(en);
		
		//System.out.println("test....."+test);
		assertEquals(6547.455744499999,test,0.455744499999);
	}

}
