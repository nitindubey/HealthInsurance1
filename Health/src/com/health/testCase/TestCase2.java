package com.health.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.BasePremium;
import com.health.Entity;

public class TestCase2 {

	@Test
	public void test() {
		
		Entity en = Entity.getInstance();
		
		en.setAge(15);
		en.setGender("male");
		en.setHyperTension("no");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("yes");
		en.setSmoking("no");
		en.setAlcohal("no");
		en.setDailyExercise("yes");
		en.setDrugs("no");
		
		BasePremium base = BasePremium.getInstance();
		
		double test=base.premimumCalc(en);
		
		//System.out.println("test....."+test);
		assertEquals(4996.47,test,0.47);
	}

}
