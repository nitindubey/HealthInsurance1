package com.health.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.BasePremium;
import com.health.Entity;

public class TestCase6 {

	@Test
	public void test() {
		
		Entity en = Entity.getInstance();
		
		en.setAge(42);
		en.setGender("female");
		en.setHyperTension("no");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("no");
		en.setSmoking("no");
		en.setAlcohal("no");
		en.setDailyExercise("no");
		en.setDrugs("no");
		
		BasePremium base = BasePremium.getInstance();
		
		double test=base.premimumCalc(en);
		
		//System.out.println("test6....."+test);
		assertEquals(8784.6,test,0.67857866052);
	}

}
