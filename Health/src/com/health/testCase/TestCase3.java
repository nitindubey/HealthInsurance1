package com.health.testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import com.health.BasePremium;
import com.health.Entity;

public class TestCase3 {

	@Test
	public void test() {
		
		Entity en = Entity.getInstance();
		
		en.setAge(22);
		en.setGender("female");
		en.setHyperTension("no");
		en.setBloodPressure("no");
		en.setBloodSugar("no");
		en.setOverWeight("no");
		en.setSmoking("no");
		en.setAlcohal("yes");
		en.setDailyExercise("yes");
		en.setDrugs("no");
		
		BasePremium base = BasePremium.getInstance();
		
		double test=base.premimumCalc(en);
		
		//System.out.println("test....."+test);
		assertEquals(5495.05,test,0.05);
	}

}
